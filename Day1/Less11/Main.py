
# a < b
def IsCorrect(a:int, b:int):
    #на одной стороне листа должны быть четное и нечетное значение
    if( a % 2 == 0 ):
        if( b % 2 != 1 ):
            return False
    else:
        if( b % 2 != 0 ):
            return False
    
    #между ними должно быть четное число страниц
    if( ( b - a - 1 ) % 4 != 0 ):
        return False
    return True

def CountPage(a:int, b:int):
    #преобразуем данные к треубемымой форме
    if( a > b ):
        c = a
        a = b
        b = c
    if( a % 2 == 1 ):
        a += 1
        b -= 1
    
    #проверяем возможность ситуации
    if( IsCorrect( a, b ) == False ):
        return 0

    result = a * 2 + b - a - 1

    return result

inputFile = open("input.txt", "r")
outputFile = open("output.txt", "w")

rawData:str = inputFile.readline()
data = rawData.split(" ")

outputFile.write( str( CountPage( int( data[0] ), int( data[1] ) ) ) )