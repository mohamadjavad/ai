import matplotlib.pyplot as plt

import numpy as np
import random
import time
import sys

MUTATE_POWER = 10
COUNT_GENERATES = 10000
POPULATION_SIZE = 150
LIFE_TIME = 10
start_time = time.time()

inputFile = open("input.txt", "r")
numTasks = int(inputFile.readline())
hardsTasks = np.array(list(map(int, inputFile.readline().split(" "))))
timesTasks = np.array(list(map(float, inputFile.readline().split(" "))))

numWorkers = int(inputFile.readline())
r = ""
for i in range(numWorkers):
    r += inputFile.readline().replace("\n", " ")
r = r.split(" ")
r = map(float, r)
koefWorkers = np.array(list(r))
koefWorkers.shape = (numWorkers, 4)


def SetKoef(a, i):
    return koefWorkers[i, int(a) - 1]


vSetKoef = np.vectorize(SetKoef)

hardMatrix = np.ones((numWorkers, numTasks))
hardMatrix *= hardsTasks
for i in range(numWorkers):
    hardMatrix[i, :] = vSetKoef(hardsTasks, i)

class Generate(object):

    def __init__(self, board=None):
        self.lifeTime = 0
        if board is None:
            self.NewGen()
        else:
            self.workersTasks = board

        self.ComputeCompliteTime()
        
    def NewGen(self):
        self.workersTasks = np.zeros(
                (numWorkers, numTasks),
                dtype=float)
        k = int(numTasks/numWorkers)
        for i in range(numTasks):
            workId = random.randint(0, numWorkers-1)
            self.workersTasks[workId, i] = 1.0
    def Iteration(self):
        self.lifeTime += 1
        if self.lifeTime > LIFE_TIME:
            self.NewGen()

    def __ge__(self, value):
        return self.compliteTime >= value.compliteTime

    def __gt__(self, value):
        return self.compliteTime > value.compliteTime

    def __le__(self, value):
        return self.compliteTime <= value.compliteTime

    def __lt__(self, value):
        return self.compliteTime < value.compliteTime

    def __eq__(self, value):
        return self.compliteTime == value.compliteTime

    def __str__(self):
        result = ""
        t = self.workersTasks.argmax(axis=0)
        t += 1
        np.set_printoptions( linewidth=np.nan )
        result += str(t) + " time: " + str(self.compliteTime)
        return result

    def SetWork(self, workerId, taskId):
        self.workersTasks[workerId, taskId] = 1.0

    def ComputeCompliteTime(self):
        resultMat = hardMatrix * self.workersTasks
        resultMat = resultMat.dot(timesTasks)
        self.compliteTime = resultMat.max()

    def GetCompliteTime(self):
        return self.compliteTime

    def Mutate(self):
        p = np.array(self.workersTasks)
        for i in range(MUTATE_POWER):
            t = random.randint(0, numTasks-1)
            np.random.shuffle(self.workersTasks[:, t])
        return Generate(p)

    def MakeChild(self, two):
        childBoard = np.array(self.workersTasks)
        for i in range(int(numTasks/2)):
            childBoard[:, i*2] = two.workersTasks[:, i*2]
        return Generate(childBoard)


def Mutate(a):
    return a.Mutate()


vMutate = np.vectorize(Mutate)


def MakeChild(a, b):
    return a.MakeChild(b)

vMakeChild = np.vectorize(MakeChild)


population = np.empty((POPULATION_SIZE), dtype=Generate)
for i in range(POPULATION_SIZE):
    population[i] = Generate()

print("--- %s seconds ---" % (time.time() - start_time))

gD = []
for i in range(COUNT_GENERATES):
    newPopulation = vMakeChild(population[:int(population.size/2)],
                               population[int(population.size/2):])
    newPopulation = np.append(newPopulation, vMutate(newPopulation))
    population = np.fmin(population, newPopulation)
    np.random.shuffle(population)
    gD.append(population.min().compliteTime)
    sys.stdout.write('\b' * 3)
    sys.stdout.write('%2s%%' % int(i / COUNT_GENERATES * 100))
    sys.stdout.flush()
g = plt.plot(gD)
plt.show()
    
    
fileOut = open("output.txt", "w")
t = population.min()
fileOut.write(str(t))
fileOut.close()

print("--- %s seconds ---" % (time.time() - start_time))

for i in range(numTasks):
    summ = np.sum(t.workersTasks[:, i])
    if summ != 1:
        print("Uncorrect unswer")
