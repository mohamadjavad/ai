from functools import reduce
import math

data:str = input()

data = data.split(" ")

data = list( map( float, filter(lambda x: x != "", data ) ) )

result = reduce(lambda b, a: math.pow(a, 2) + b, data, 0)

print( math.sqrt( result ) )


