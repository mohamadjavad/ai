from functools import reduce
import math

data:str = input()

data = data.split(" ")

data = list( map( int, filter(lambda x: x != "", data ) ) )

resultData = []

tmp = None

for i in data:
    if tmp != None:
        if i != 0:
            resultData.append( tmp )
    tmp = i

resultData.append( tmp )

result = ""
for i in resultData:
    result += str( i )
    result += " "

print( result )
    


