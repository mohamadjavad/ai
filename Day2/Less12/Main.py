import re

regex = r"[a-zA-Z]+"

result = 0

inputFile = open( "d.in", "r" )
data = inputFile.readline()

matches = re.finditer( regex, data )

for match in matches:
    result += 1

regex = r"[a-zA-Z]-[a-zA-Z]"

matches = re.finditer( regex, data )

for match in matches:
    result -= 1

outputFile = open( "d.out", "w" )

outputFile.write( str( result ) )
